require 'nn'
require 'rnn'  -- IMP: dont use LSTM package from nnx - buggy
require 'nngraph'

--require 'cunn'
-- IMP if args is not passed, it takes from global 'args'
return function(args)

    function create_network(args)
        local n_hid = 100
        local mlp = nn.Sequential()
        -- duplicate the input 
        local dup = nn.ConcatTable()
        dup:add(nn.Identity())
        dup:add(nn.Identity())
        
        mlp:add(dup)
        
        -- run action and object paths in parallel
        local double_path = nn.ParallelTable()

        
        local action_net = nn.Sequential()

        
        -- Calculate attention coef for the words
        local action_attn = nn.Sequential()
        action_attn:add(nn.Reshape(args.hist_len*args.ncols*args.state_dim))
        action_attn:add(nn.Linear(args.hist_len*args.ncols*args.state_dim, args.hist_len*args.ncols*args.state_dim)) 
        action_attn:add(nn.Tanh())
        action_attn:add(nn.SoftMax())

        local action_concat = nn.ConcatTable()
        action_concat:add(action_attn)
        action_concat:add(nn.Identity())
        
        action_net:add(action_concat)
        -- Multiply each word with its attention coefs 
        action_net:add(nn.CMulTable())

        action_net:add(nn.Reshape(args.hist_len*args.ncols*args.state_dim))
        action_net:add(nn.Linear(args.hist_len*args.ncols*args.state_dim, n_hid))
        action_net:add(nn.Rectifier())
        action_net:add(nn.Linear(n_hid, n_hid))
        action_net:add(nn.Rectifier())

        local object_net = nn.Sequential()
        
        -- Calculate attention coef for the words
        local object_attn = nn.Sequential()
        object_attn:add(nn.Reshape(args.hist_len*args.ncols*args.state_dim))
        object_attn:add(nn.Linear(args.hist_len*args.ncols*args.state_dim, args.hist_len*args.ncols*args.state_dim)) 
        object_attn:add(nn.Tanh())
        object_attn:add(nn.SoftMax())

        local object_concat = nn.ConcatTable()
        object_concat:add(object_attn)
        object_concat:add(nn.Identity())
        
        object_net:add(object_concat)
        -- Multiply each word with its attention coefs 
        object_net:add(nn.CMulTable())

        object_net:add(nn.Reshape(args.hist_len*args.ncols*args.state_dim))
        object_net:add(nn.Linear(args.hist_len*args.ncols*args.state_dim, n_hid))
        object_net:add(nn.Rectifier())
        object_net:add(nn.Linear(n_hid, n_hid))
        object_net:add(nn.Rectifier())
        
        
        -- add the two paths to the parallel stream
        double_path:add(action_net)
        double_path:add(object_net)
        mlp:add(double_path)

        -- Neural Q-Network
        local mlp_out = nn.ParallelTable()
        mlp_out:add(nn.Linear(n_hid, args.n_actions))
        mlp_out:add(nn.Linear(n_hid, args.n_objects))

        mlp:add(mlp_out)
        print('Full Model')
        print(mlp)
        if args.gpu >=0 then
            mlp:cuda()
        end    
        return mlp
    end

    return create_network(args)
end

