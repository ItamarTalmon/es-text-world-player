-- agent
require 'torch'

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Train Agent in Environment:')
cmd:text()
cmd:text('Options:')

cmd:option('-exp_folder', '', 'name of folder where current exp state is being stored')
cmd:option('-text_world_location', '', 'location of text-world folder')

cmd:option('-framework', '', 'name of training framework')

cmd:option('-env', '', 'name of environment to use')
cmd:option('-env_params', '', 'string of environment parameters')
cmd:option('-actrep', 1, 'how many times to repeat action')
cmd:option('-random_starts', 0, 'play action 0 between 1 and random_starts ' ..
           'number of times at the start of each training episode')

cmd:option('-name', '', 'filename used for saving network and training history')
cmd:option('-network', '', 'reload pretrained network')
cmd:option('-agent', '', 'name of agent file to use')
cmd:option('-agent_params', '', 'string of agent parameters')
cmd:option('-seed', 1, 'fixed input seed for repeatable experiments')
cmd:option('-saveNetworkParams', false,
           'saves the agent network in a separate file')

cmd:option('-pre_trained', 0,'use word2vec pretrained vectors')
cmd:option('-recurrent', 0,'bow or recurrent')
cmd:option('-bigram', 0,'bigram version')
cmd:option('-quest_levels', 1,'# of quests to complete in each run')
cmd:option('-state_dim', 100, 'max dimensionality of raw state (stream of symbols or BOW vocab)')
cmd:option('-max_steps', 100,'max steps per episode')


cmd:option('-prog_freq', 5*10^3, 'frequency of progress output')
cmd:option('-save_freq', 5*10^4, 'the model is saved every save_freq steps')
cmd:option('-eval_freq', 10^4, 'frequency of greedy evaluation')
cmd:option('-save_versions', 0, '')

cmd:option('-steps', 10^5, 'number of training steps to perform')
cmd:option('-eval_steps', 10^5, 'number of evaluation steps')

cmd:option('-verbose', 2,
           'the higher the level, the more information is printed to screen')
cmd:option('-threads', 1, 'number of BLAS threads')
cmd:option('-gpu', -1, 'gpu flag')
cmd:option('-game_num', 1, 'game number (for parallel game servers)')
cmd:option('-wordvec_file', 'wordvec.eng' , 'Word vector file')
cmd:option('-tutorial_world', 1, 'play tutorial_world')
cmd:option('-random_test', 0, 'test random policy')
cmd:option('-analyze_test', 0, 'load model and analyze')
cmd:option('-use_wordvec', 0, 'use word vec')
cmd:option('-train', 1, 'is we wish to train (if false we will not update the network parameters)')
cmd:text()



local opt = cmd:parse(arg)
print("opt.game_num = ",opt.game_num)
local exp_folder_root="logs/run"..opt.game_num.."/"
local master_folder="logs/master/"

print(opt)
TRAINED = (opt.train==1)
PRE_TRAINED = opt.pre_trained

RECURRENT = opt.recurrent
BIGRAM = opt.bigram
QUEST_LEVELS = opt.quest_levels
STATE_DIM = opt.state_dim
MAX_STEPS = opt.max_steps
WORDVEC_FILE = opt.wordvec_file
TUTORIAL_WORLD = (opt.tutorial_world==1)
RANDOM_TEST = (opt.random_test==1)
ANALYZE_TEST = (opt.analyze_test==1)

WORD2VEC_DIM = 300
if PRE_TRAINED == 1 then
    local pre_trained_path = 'GoogleNews-vectors-negative300-SLIM_adapted.t7'
    print('Using pre trained  vectors from file : ' .. pre_trained_path)
    word2vec_file = torch.load(pre_trained_path)
    STATE_DIM = WORD2VEC_DIM
    end


print(STATE_DIM)
print("Tutorial world", TUTORIAL_WORLD)

require 'client'
require 'utils'
require 'xlua'
require 'optim'

local framework
if TUTORIAL_WORLD then
    framework = require 'framework_fantasy'
else
    framework = require 'framework'
end

---------------------------------------------------------------

if not dqn then
    dqn = {}
    require 'nn'
    require 'nngraph'
    require 'nnutils'
    -- require 'Scale'
    require 'NeuralQLearner'
    require 'TransitionTable'
    require 'Rectifier'
    require 'Embedding'
end
--  agent login
local port = 4000 + opt.game_num
print(port)
client_connect(port)
login('root', 'root')

if TUTORIAL_WORLD then
    framework.makeSymbolMapping(opt.text_world_location .. 'evennia/contrib/tutorial_world/build.ev')
else
    framework.makeSymbolMapping(opt.text_world_location .. 'evennia/contrib/text_sims/build.ev')
end

print("#symbols", #symbols)

EMBEDDING.weight[#symbols+1]:mul(0) --zero out NULL INDEX vector

-- init with word vec
if opt.use_wordvec==1 then
    print(WORDVEC_FILE)
    local wordVec = readWordVec(WORDVEC_FILE)
    print(#wordVec)
    for i=1, #symbols do
        print("wordvec", symbols[i], wordVec[symbols[i]])
        EMBEDDING.weight[i] = torch.Tensor(wordVec[symbols[i]])
        assert(EMBEDDING.weight[i]:size(1) == n_hid)
    end
else
    for i=1, #symbols do
        EMBEDDING.weight[i] = torch.rand(EMBEDDING.weight[i]:size(1))*0.02-0.01
    end
end

--- General setup.
if opt.agent_params then
    opt.agent_params = str_to_table(opt.agent_params)
    opt.agent_params.gpu       = opt.gpu
    opt.agent_params.best      = opt.best
    opt.agent_params.verbose   = opt.verbose
    if opt.network ~= '' then
        opt.agent_params.network = opt.network
    end

    opt.agent_params.actions = framework.getActions()
	opt.agent_params.objects = framework.getObjects()

    if RECURRENT == 0 then
        if vector_function == convert_text_to_bow2 then
            opt.agent_params.state_dim = 2 * (#symbols)
            if PRE_TRAINED == 1 then
                    opt.agent_params.state_dim = (2*WORD2VEC_DIM)
            end
        elseif vector_function == convert_text_to_bigram then
            if TUTORIAL_WORLD then
                opt.agent_params.state_dim = (#symbols*5)
                if PRE_TRAINED == 1 then
                    opt.agent_params.state_dim = (WORD2VEC_DIM*5)
                end
            else
                opt.agent_params.state_dim = (#symbols*#symbols)
                if PRE_TRAINED == 1 then
                    opt.agent_params.state_dim = (WORD2VEC_DIM*WORD2VEC_DIM)
                end
            end
        else

            opt.agent_params.state_dim = (#symbols)
            if PRE_TRAINED == 1 then
                    opt.agent_params.state_dim = WORD2VEC_DIM
            end
        end
    end
end
print("state_dim", opt.agent_params.state_dim)

local agent = dqn[opt.agent](opt.agent_params) -- calls dqn.NeuralQLearner:init


-- override print to always flush the output
local old_print = print
local print = function(...)
    old_print(...)
    io.flush()
end
-- local params, gradParams = agent.network:parameters()
local p, gp = agent.network:getParameters()


function calculate_update_value (sigma, parameter_value)
  return parameter_value + (torch.normal() * sigma)
end

function parse_reward()
  file_name = exp_folder_root..'test_avgR.log'
  file =  io.lines(exp_folder_root..'test_avgR.log')
  file()
  reward = file()
  print("extracted reward = ",reward,"from log file", file_name)
  return reward
end

agent.network:zeroGradParameters() -- make sure all gp numbers are zeros
local p, gp = agent.network:getParameters()
file_name = master_folder..'master_parameters'
updated_parameters_from_file = torch.load(file_name)

local number_of_parameters = p:size()[1]
local percent_of_parameters = 0.05
local number_of_parameters_to_change = math.floor(percent_of_parameters * number_of_parameters)
local sigma = 0.1    -- noise standard deviation
print("taking ",percent_of_parameters,"percent of ",number_of_parameters_to_change, " parameters")
print("starting random parameters updating loop")
for i=1, number_of_parameters_to_change do
    local parameter_index = math.random(number_of_parameters)
    random_update_value = calculate_update_value(sigma, p[parameter_index])
    updated_parameters_from_file[parameter_index] = updated_parameters_from_file[parameter_index] + random_update_value
    gp[parameter_index] = random_update_value
end
print("finished loop")

file_name = exp_folder_root..'updated_parameters'
torch.save(file_name, updated_parameters_from_file)

print("************************Now running game***********************")
os.execute("./run_cpu_evolutionary "..opt.game_num)

function update_parameters_multiplied_with_reward(gp)
  reward = parse_reward()
  normalized_reward = (reward + 1.5)/ 3
  for i=1, number_of_parameters do
    if gp[i] ~= 0 then
      gp[i] = gp[i] * normalized_reward
    end
    return gp
  end
end

gp = update_parameters_multiplied_with_reward(gp)
file_name = exp_folder_root..'params_diff_with_reward'..opt.game_num
torch.save(file_name,gp)

