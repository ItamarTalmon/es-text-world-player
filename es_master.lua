-- agent
require 'torch'

-- override print to always flush the output
local old_print = print
local print = function(...)
    old_print('\n-------------------------------------------------------')
    old_print(...)
	old_print('-------------------------------------------------------\n')
    io.flush()
end

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Train Agent in Environment:')
cmd:text()
cmd:text('Options:')

cmd:option('-exp_folder', '', 'name of folder where current exp state is being stored')
cmd:option('-text_world_location', '', 'location of text-world folder')

cmd:option('-framework', '', 'name of training framework')

cmd:option('-env', '', 'name of environment to use')
cmd:option('-env_params', '', 'string of environment parameters')
cmd:option('-actrep', 1, 'how many times to repeat action')
cmd:option('-random_starts', 0, 'play action 0 between 1 and random_starts ' ..
           'number of times at the start of each training episode')

cmd:option('-name', '', 'filename used for saving network and training history')
cmd:option('-network', '', 'reload pretrained network')
cmd:option('-agent', '', 'name of agent file to use')
cmd:option('-agent_params', '', 'string of agent parameters')
cmd:option('-seed', 1, 'fixed input seed for repeatable experiments')
cmd:option('-saveNetworkParams', false,
           'saves the agent network in a separate file')

cmd:option('-pre_trained', 0,'use word2vec pretrained vectors')
cmd:option('-recurrent', 0,'bow or recurrent')
cmd:option('-bigram', 0,'bigram version')
cmd:option('-quest_levels', 1,'# of quests to complete in each run')
cmd:option('-state_dim', 100, 'max dimensionality of raw state (stream of symbols or BOW vocab)')
cmd:option('-max_steps', 100,'max steps per episode')


cmd:option('-prog_freq', 5*10^3, 'frequency of progress output')
cmd:option('-save_freq', 5*10^4, 'the model is saved every save_freq steps')
cmd:option('-eval_freq', 10^4, 'frequency of greedy evaluation')
cmd:option('-save_versions', 0, '')

cmd:option('-steps', 10^5, 'number of training steps to perform')
cmd:option('-eval_steps', 10^5, 'number of evaluation steps')

cmd:option('-verbose', 2,
           'the higher the level, the more information is printed to screen')
cmd:option('-threads', 1, 'number of BLAS threads')
cmd:option('-gpu', -1, 'gpu flag')
cmd:option('-game_num', 1, 'game number (for parallel game servers)')
cmd:option('-wordvec_file', 'wordvec.eng' , 'Word vector file')
cmd:option('-tutorial_world', 1, 'play tutorial_world')
cmd:option('-random_test', 0, 'test random policy')
cmd:option('-analyze_test', 0, 'load model and analyze')
cmd:option('-use_wordvec', 0, 'use word vec')
cmd:option('-workers_num', 4, 'the number of threads (Evolutionary Workers) we wish to run')
cmd:option('-loop_num', 2, 'the number of threads (Evolutionary Workers) we wish to run')


cmd:text()

local opt = cmd:parse(arg)
print(opt)
PRE_TRAINED = opt.pre_trained
if PRE_TRAINED == 1 then
    local pre_trained_path = 'GoogleNews-vectors-negative300-SLIM_adapted.t7'
    print('Using pre trained  vectors from file : ' .. pre_trained_path)
    word2vec_file = torch.load(pre_trained_path)
    end
RECURRENT = opt.recurrent
BIGRAM = opt.bigram
QUEST_LEVELS = opt.quest_levels
STATE_DIM = opt.state_dim
MAX_STEPS = opt.max_steps
WORDVEC_FILE = opt.wordvec_file
TUTORIAL_WORLD = (opt.tutorial_world==1)
RANDOM_TEST = (opt.random_test==1)
ANALYZE_TEST = (opt.analyze_test==1)

WORD2VEC_DIM = 300 * (BIGRAM + 1)
if PRE_TRAINED == 1 then
    local pre_trained_path = 'GoogleNews-vectors-negative300-SLIM_adapted.t7'
    print('Using pre trained  vectors from file : ' .. pre_trained_path)
    word2vec_file = torch.load(pre_trained_path)
    STATE_DIM = WORD2VEC_DIM
    end

-- Get the number of ES Workers
WORKERS_NUM	= opt.workers_num
MASTER_THREAD_NUM = opt.workers_num + 1
	
print('Master will run with ' .. WORKERS_NUM .. ' Parallel ES Workers')

require 'client'
require 'utils'
require 'xlua'
require 'optim'

local framework
if TUTORIAL_WORLD then
    framework = require 'framework_fantasy'
else
    framework = require 'framework'
end

---------------------------------------------------------------

if not dqn then
    dqn = {}
    require 'nn'
    require 'nngraph'
    require 'nnutils'
    -- require 'Scale'
    require 'NeuralQLearner'
    require 'TransitionTable'
    require 'Rectifier'
    require 'Embedding'
end
--  agent login
local port = 4000 + MASTER_THREAD_NUM -- Master thread needs a game as well!
print('Master is conncted to port number ' .. port)
client_connect(port)
login('root', 'root')

if TUTORIAL_WORLD then
    framework.makeSymbolMapping(opt.text_world_location .. 'evennia/contrib/tutorial_world/build.ev')
else
    framework.makeSymbolMapping(opt.text_world_location .. 'evennia/contrib/text_sims/build.ev')
end

print("#symbols", #symbols)

EMBEDDING.weight[#symbols+1]:mul(0) --zero out NULL INDEX vector


--- General setup.
if opt.agent_params then
    opt.agent_params = str_to_table(opt.agent_params)
    opt.agent_params.gpu       = opt.gpu
    opt.agent_params.best      = opt.best
    opt.agent_params.verbose   = opt.verbose
    if opt.network ~= '' then
        opt.agent_params.network = opt.network
    end

    opt.agent_params.actions = framework.getActions()
	opt.agent_params.objects = framework.getObjects()

    if RECURRENT == 0 then
        if vector_function == convert_text_to_bow2 then
            opt.agent_params.state_dim = 2 * (#symbols)
            if PRE_TRAINED == 1 then 
                    opt.agent_params.state_dim = (2*WORD2VEC_DIM)
            end
        elseif vector_function == convert_text_to_bigram then
            if TUTORIAL_WORLD then
                opt.agent_params.state_dim = (#symbols*5)
                if PRE_TRAINED == 1 then
                    opt.agent_params.state_dim = (WORD2VEC_DIM*5)
                end
            else
                opt.agent_params.state_dim = (#symbols*#symbols)
                if PRE_TRAINED == 1 then 
                    opt.agent_params.state_dim = (WORD2VEC_DIM*WORD2VEC_DIM)
                end
            end
        else
            
            opt.agent_params.state_dim = (#symbols)
            if PRE_TRAINED == 1 then 
                    opt.agent_params.state_dim = WORD2VEC_DIM
            end
        end
    end
end
print("state_dim", opt.agent_params.state_dim)

local agent = dqn[opt.agent](opt.agent_params) -- calls dqn.NeuralQLearner:init


-- save master parameters
function save_master_params()
	agent.network:zeroGradParameters() -- make sure all gp numbers are zeros
	local p, gp = agent.network:getParameters()
	file_name = opt.exp_folder..'master_parameters'
	torch.save(file_name,gp)
end

save_master_params()


local learn_start = agent.learn_start
local start_time = sys.clock()
local reward_counts = {}
local episode_counts = {}
local time_history = {}
local v_history = {}
local qmax_history = {}
local bestq_history = {}
local td_history = {}
local reward_history = {}
local step = 0
time_history[1] = 0

function parse_reward()
  file_name = 'logs/run1/test_avgR.log'
  file =  io.lines('logs/run1/test_avgR.log')
  file()
  reward = file()
  print("extracted reward = ",reward,"from log file", file_name)
  return reward
end

function clean_reward_log()
   -- Opens a file in append mode
	file = io.open(opt.exp_folder.."test_avgR.log", "w")
	-- appends a word test to the last line of the file
	file:write('')
	-- closes the open file
	file:close()
end
-- clean reward file
clean_reward_log()

function write_reward_to_log(reward)
   -- Opens a file in append mode
	file = io.open(opt.exp_folder.."test_avgR.log", "a")
	-- appends a word test to the last line of the file
	file:write(reward..'\n')
	-- closes the open file
	file:close()
end

function evaluate_model()
   file_name = 'logs/run1/updated_parameters'
   torch.save(file_name, updated_parameters_from_file)  
   os.execute("./run_cpu_evolutionary 1")
   reward = parse_reward()
   print('Current Reward is '..reward)
   write_reward_to_log(reward)
end



print("Started Evolutionary Strategies based training")


function thrd()
	print('Running Evolutionary Worker No. '.. __threadid)
	-- os.execute('echo \"' .. __threadid .. ' is sleeping\" && sleep 2 ')
	os.execute('./load_n ' .. __threadid)
	return __threadid
end

-- A simple example is better than convoluted explanations:

local threads = require 'threads'

local LOOP_NUM = opt.loop_num
local msg = "hello from a master thread"

-- create the thread pool
local pool = threads.Threads(
   WORKERS_NUM,
   -- this function will run on every new thread once
   function(threadid)
      print('starting a new thread number ' .. threadid)
      gmsg = msg -- get it the msg upvalue and store it in thread state
   end
)

local jobdone = 0

require 'os'

-- in every loop, every thread will do one job
for j=1,LOOP_NUM do
        
	print('Master is calling Evolutionary Workers (Loop Number ' .. j .. ')' )
	for i=1,WORKERS_NUM do

	   pool:addjob(
	      -- this is the job we wish the thread todo
	      thrd,
		  -- this function will run when the job is done
	      function(id)
	         print(string.format("Evolutionary Worker %x finished", id))
	         jobdone = jobdone + 1
	      end
	   )
	end

	-- Wait for all current jobs in the pool to finish
	pool:synchronize()
	print('All Evolutionary Workers are done... ( Loop Number ' .. j .. ' )' )
	print(string.format('%d workers jobs done (in total)', jobdone))
	
	print('Master is collecting new parameters from threads...')
	-- collect workers updated parameters

	for i=1,WORKERS_NUM do
	   print('Master is updating its model with the diff from thread '..i)	
	   local p, gp = agent.network:getParameters()
	   local updated_parameters = torch.load(opt.exp_folder.."/../run"..i.."/params_diff_with_reward"..i)
	   p = p + updated_parameters
	end

	print('Master is saving its new parameters')		
	-- save new master parameters
	save_master_params()

	print('Master is evaluating its new model')		
	-- run validation and save reward
	evaluate_model()

	print('Loop number ' .. j .. ' is done!' )

end

print('ES training is done, terminating workers')
-- Kill Threads
pool:terminate()
print('All Evolutionary Workers are terminated')




